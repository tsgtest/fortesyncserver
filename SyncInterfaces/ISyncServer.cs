using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace LMP.MacPac.Sync
{
    public interface ISyncServer
    {
        void PublishUserSyncData(int iUserID, DataSet oSourceData, string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        DataSet GetUserSyncData(int iUserID, string xLastSyncDown, string xServer, string xDatabase, bool bOnDemand, string xSystemLoginID, string xClientMachine);
        DataSet GetUserSyncPeopleData(int iUserID, string xLastSyncDown, string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        DataSet GetSharedSegments(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        void PublishAdminSyncData(DataSet oSourceData, string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        DataSet GetAdminSyncData(string xLastSyncDown, string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        void CopyNetworkDB(string xSourceServer, string xSourceDatabase,
            string xDestServer, string xDestDatabase, string xDestLogin, string xDestPassword, string xSystemLoginID, string xClientMachine);
        int GetUserID(string xSystemLoginID, string xServer, string xDatabase, string xClientMachine);
        DateTime GetAdminPublishedTime(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        void SetAdminPublishedTime(string xServer, string xDatabase, string xDate, string xSystemLoginID, string xClientMachine);
        bool AdminPublishInProgress(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        void StartAdminPublish(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        void EndAdminPublish(string xServer, string xDatabase, string xPublishingID, string xSystemLoginID, string xClientMachine);
        void LogEvent(string xMessage);
        void BuildCache();
        bool SyncAvailable(string xServer, string xDatabase, string xIISAddress, string xSystemLoginID, string xClientMachine);
        string[] PeopleCustomFieldList(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        int GetUserOfficeID(string xSystemLoginID, string xServer, string xDatabase);
        string GetRequiredMinClientVersion(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        void SetRequiredMinClientVersion(string xServer, string xDatabase, string xValue, string xSystemLoginID, string xClientMachine);
        string GetServerGUID(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
        string GetLastPublishID(string xServer, string xDatabase, string xSystemLoginID, string xClientMachine);
    }
}
