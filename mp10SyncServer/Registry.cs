using System;
using System.Reflection;
using Reg = Microsoft.Win32.Registry;
using Win32 = Microsoft.Win32;

namespace LMP
{
	/// <summary>
	/// Summary description for Registry.
	/// </summary>
	public static class Registry
	{
        /// <summary>
        /// returns the path to the MacPac 10 root
        /// </summary>
        public static string MacPac10RegistryRoot
        {
            get { return @"Software\The Sackett Group\Deca"; }
        }

        /// <summary>
        /// returns the path to the CI root
        /// </summary>
        public static string CIRegistryRoot
        {
            get { return @"Software\The Sackett Group\CI"; }
        }

        /// <summary>
        /// returns the path to the Numbering root
        /// </summary>
        public static string NumberingRegistryRoot
        {
            get { return @"Software\The Sackett Group\Numbering"; }
        }

        /// <summary>
        /// returns a MacPac 10 registry value
        /// </summary>
        /// <param name="xValueName"></param>
        /// <returns></returns>
        public static string GetMacPac10Value(string xValueName)
        {
            return GetLocalMachineValue(MacPac10RegistryRoot, xValueName);
        }

        /// <summary>
        /// sets the value of a MacPac registry item
        /// </summary>
        /// <param name="xValueName"></param>
        /// <param name="xValue"></param>
        /// <returns></returns>
        public static void SetMacPac10Value(string xValueName, string xValue)
        {
            SetCurrentUserValue(MacPac10RegistryRoot, xValueName, xValue);
        }

		/// <summary>
		/// returns the value of the specified local machine value -
		/// returns null if string does not exist
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <returns></returns>
		public static string GetLocalMachineValue(string xSubKey, string xName)
		{
			return GetValue(Reg.LocalMachine, xSubKey, xName);
		}

		public static string GetLocalMachineValue(string xSubKey)
		{
			return GetValue(Reg.LocalMachine, xSubKey, null);
		}

		/// <summary>
		/// returns the value of the specified Classes Root value -
		/// returns null if string does not exist
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <returns></returns>
		public static string GetClassesRootValue(string xSubKey, string xName)
		{
			return GetValue(Reg.ClassesRoot, xSubKey, xName);
		}

		public static string GetClassesRootValue(string xSubKey)
		{
			return GetValue(Reg.ClassesRoot, xSubKey, null);
		}


		/// <summary>
		/// returns the value of the specified Current User value -
		/// returns null if string does not exist
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <returns></returns>
		public static string GetCurrentUserValue(string xSubKey, string xName)
		{
			return GetValue(Reg.CurrentUser, xSubKey, xName);
		}

		public static string GetCurrentUserValue(string xSubKey)
		{
			return GetValue(Reg.CurrentUser, xSubKey, null);
		}


		/// <summary>
		/// returns the value of the specified Current Config value -
		/// returns null if string does not exist
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <returns></returns>
		public static string GetCurrentConfigValue(string xSubKey, string xName)
		{
			return GetValue(Reg.CurrentConfig, xSubKey, xName);
		}

		public static string GetCurrentConfigValue(string xSubKey)
		{
			return GetValue(Reg.CurrentConfig, xSubKey, null);
		}

		/// <summary>
		/// sets the value of the specified current user value
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		public static void SetCurrentUserValue(string xSubKey, string xName, string xValue)
		{
			SetValue(Reg.CurrentUser, xSubKey, xName, xValue);
		}

		public static void SetCurrentUserValue(string xSubKey, string xValue)
		{
			SetValue(Reg.CurrentUser, xSubKey, null, xValue);
		}

		/// <summary>
		/// sets the value of the specified local machine value
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		public static void SetLocalMachineValue(string xSubKey, string xName, string xValue)
		{
			SetValue(Reg.LocalMachine, xSubKey, xName, xValue);
		}

		public static void SetLocalMachineValue(string xSubKey, string xValue)
		{
			SetValue(Reg.LocalMachine, xSubKey, null, xValue);
		}

		/// <summary>
		/// sets the value of the specified classes root value
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		public static void SetClassesRootValue(string xSubKey, string xName, string xValue)
		{
			SetValue(Reg.ClassesRoot, xSubKey, xName, xValue);
		}

		public static void SetClassesRootValue(string xSubKey, string xValue)
		{
			SetValue(Reg.ClassesRoot, xSubKey, null, xValue);
		}

		/// <summary>
		/// sets the value of the specified current config value
		/// </summary>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		public static void SetCurrentConfigValue(string xSubKey, string xName, string xValue)
		{
			SetValue(Reg.CurrentConfig, xSubKey, xName, xValue);
		}

		public static void SetCurrentConfigValue(string xSubKey, string xValue)
		{
			SetValue(Reg.CurrentConfig, xSubKey, null, xValue);
		}

        /// <summary>
        /// deletes the specified value
        /// </summary>
        /// <param name="xSubKey"></param>
        /// <param name="xValue"></param>
        public static void DeleteValue(string xSubKey, string xValue)
        {
            Microsoft.Win32.Registry.CurrentUser.DeleteValue(xSubKey + @"\" + xValue);
        }
	
		/// <summary>
		/// returns the text of the specified value -
		/// returns null if string does not exist
		/// </summary>
		/// <param name="oTopLevelKey"></param>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <returns></returns>
		private static string GetValue(Win32.RegistryKey oTopLevelKey, string xSubKey,
			string xName)
		{
			string xRetValue = null;
			Win32.RegistryKey oSubKey;

			//open specified sub key
			try
			{
				oSubKey= oTopLevelKey.OpenSubKey(@xSubKey);
			}
			catch (System.Exception e)
			{
				//error opening key
                throw new System.Exception("An error occurred when attempting to open the following registry key: " + @xSubKey, e);
			}
			
			if(oSubKey == null)
				//no key found - return null
				return null;
			else
			{
				try
				{
					//get value of specified value
					xRetValue = oSubKey.GetValue(xName, "NULL").ToString();
                    if(xRetValue == "NULL")
                        return null;
                    else
					    return xRetValue;
				}
				catch
				{
					//specified value does not exist
					return null;
				}
				finally
				{
					oSubKey.Close();
				}
			}
		}

		/// <summary>
		/// sets the specified value -
		/// throws exception if key doesn't exist
		/// </summary>
		/// <param name="oTopLevelKey"></param>
		/// <param name="xSubKey"></param>
		/// <param name="xName"></param>
		/// <param name="xValue"></param>
		private static void SetValue(Win32.RegistryKey oTopLevelKey, string xSubKey,
			string xName, string xValue)
		{
			Win32.RegistryKey oSubKey;

			//open key with write access
			try
			{
                //Use CreateSubKey instead of OpenSubKey so that
                //missing Key will be created
                oSubKey = oTopLevelKey.CreateSubKey(@xSubKey);
			}
			catch (System.Exception e)
			{
				//error opening key
				throw new System.Exception
					("An error occurred when attempting to open the following registry key with write access: " + @xSubKey, e);
			}

			if (oSubKey == null)
			{
                //requested key does not exist
				throw new System.Exception
					("The following registry key does not exist: " + @xSubKey);
			}

			//set value
			try
			{
				oSubKey.SetValue(xName, xValue);
			}
			catch (System.Exception e)
			{
				//error setting value
				throw new System.Exception
					("An error occurred when  attempting to set the following registry value: " + @xSubKey + "\\" + xName, e);
			}
			finally
			{
				oSubKey.Close();
			}
		}

	}
}
